package com.github.axet.catalogsreader.app;

import android.content.Context;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;

import com.github.axet.androidlibrary.app.MainApplication;
import com.github.axet.androidlibrary.app.Storage;
import com.github.axet.androidlibrary.net.HttpClient;
import com.github.axet.catalogsreader.R;

import org.geometerplus.zlibrary.ui.android.library.ZLAndroidApplication;

import java.io.File;

public class CatalogsApplication extends MainApplication {
    final String TAG = CatalogsApplication.class.getSimpleName();

    public static String SCHEME_MAGNET = "magnet";

    public static final String PREFERENCE_STORAGE = "storage_path";
    public static final String PREFERENCE_SCREENLOCK = "screen_lock";
    public static final String PREFERENCE_THEME = "theme";

    public static final String PREFERENCE_PROXY = "proxy";
    public static final String PREFERENCE_PROXY_PREFIX = "proxy_";

    public static String PREFERENCE_CATALOGS = "catalogs";
    public static String PREFERENCE_CATALOGS_PREFIX = "catalogs_";
    public static String PREFERENCE_CATALOGS_COUNT = "count";

    public ZLAndroidApplication zlib;
    public EnginesManager engines;

    public static CatalogsApplication from(Context context) {
        return (CatalogsApplication) MainApplication.from(context);
    }

    public static int getTheme(Context context, int light, int dark) {
        return MainApplication.getTheme(context, PREFERENCE_THEME, light, dark);
    }

    public static String onTrimString(int level) {
        switch (level) {
            case TRIM_MEMORY_COMPLETE:
                return "TRIM_MEMORY_COMPLETE";
            case TRIM_MEMORY_MODERATE:
                return "TRIM_MEMORY_MODERATE";
            case TRIM_MEMORY_BACKGROUND:
                return "TRIM_MEMORY_BACKGROUND";
            case TRIM_MEMORY_UI_HIDDEN:
                return "TRIM_MEMORY_UI_HIDDEN";
            case TRIM_MEMORY_RUNNING_CRITICAL:
                return "TRIM_MEMORY_RUNNING_CRITICAL";
            case TRIM_MEMORY_RUNNING_LOW:
                return "TRIM_MEMORY_RUNNING_LOW";
            case TRIM_MEMORY_RUNNING_MODERATE:
                return "TRIM_MEMORY_RUNNING_MODERATE";
        }
        return "unknown";
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
        PreferenceManager.setDefaultValues(this, R.xml.pref_general, false);
        zlib = new ZLAndroidApplication() {
            {
                attachBaseContext(CatalogsApplication.this);
                onCreate();
            }
        };
        engines = new EnginesManager(this);
        engines.load();
        new HttpClient.SpongyLoader(this, false);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        Log.d(TAG, "onTerminate");
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Log.d(TAG, "onLowMemory");
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Log.d(TAG, "onTrimMemory: " + onTrimString(level));
    }

    public File getCache() {
        File cache = getExternalCacheDir();
        if (cache == null || !Storage.canWrite(cache))
            cache = getCacheDir();
        return cache;
    }
}
