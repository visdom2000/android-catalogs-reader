package com.github.axet.catalogsreader.dialogs;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.github.axet.androidlibrary.net.HttpClient;
import com.github.axet.androidlibrary.widgets.DialogFragmentCompat;
import com.github.axet.androidlibrary.widgets.ErrorDialog;
import com.github.axet.androidlibrary.widgets.WebViewCustom;
import com.github.axet.bookreader.app.Storage;
import com.github.axet.catalogsreader.activities.MainActivity;
import com.github.axet.catalogsreader.app.CatalogsApplication;
import com.github.axet.catalogsreader.net.HttpProxyClient;
import com.github.axet.catalogsreader.widgets.Drawer;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.nio.charset.Charset;

public class OpenIntentDialogFragment extends DialogFragmentCompat {

    Handler handler = new Handler();
    Thread t;

    public static String loadJson(Context context, Uri uri) {
        try {
            String json;
            if (uri.getScheme().equals(ContentResolver.SCHEME_FILE)) {
                InputStream is = context.getContentResolver().openInputStream(uri);
                json = IOUtils.toString(is, Charset.defaultCharset());
            } else if (uri.getScheme().equals(ContentResolver.SCHEME_ANDROID_RESOURCE)) { // app assests
                InputStream is = context.getContentResolver().openInputStream(uri);
                json = IOUtils.toString(is, Charset.defaultCharset());
            } else if (Build.VERSION.SDK_INT >= 21 && uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) { // saf
                Storage.takePersistableUriPermission(context, uri, Storage.SAF_RW);
                InputStream is = context.getContentResolver().openInputStream(uri);
                json = IOUtils.toString(is, Charset.defaultCharset());
            } else {
                HttpProxyClient client = new HttpProxyClient();
                HttpClient.DownloadResponse w = client.getResponse(null, uri.toString());
                w.download();
                if (w.getError() != null)
                    throw new RuntimeException(w.getError() + ": " + uri.toString());
                json = w.getHtml();
            }
            return json;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        final MainActivity main = (MainActivity) getActivity();

        t = new Thread("Delayed Intent") {
            @Override
            public void run() {
                try {
                    openURL(getArguments().getString("url"));
                } catch (final RuntimeException e) {
                    ErrorDialog.Post(main, e);
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (main.isFinishing())
                            return;
                        dismissAllowingStateLoss(); //  Can not perform this action after onSaveInstanceState
                    }
                });
            }
        };
        t.start();
    }

    public void openURL(final String url) {
        final MainActivity main = (MainActivity) getActivity();
        if (main == null || main.isFinishing()) // when app was destoryed
            return;
        if (url.startsWith(CatalogsApplication.SCHEME_MAGNET)) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (main.isFinishing())
                        return;
                    CatalogsApplication app = CatalogsApplication.from(getContext());
                    app.engines.addManget(main, url);
                }
            });
            return;
        }
        if (url.startsWith(Drawer.SCHEME_CATALOG)) {
            Uri uri = Uri.parse(url).buildUpon().scheme(WebViewCustom.SCHEME_HTTP).build();
            String json = loadJson(getContext(), uri);
            try {
                CatalogsApplication app = CatalogsApplication.from(getContext());
                JSONObject obj = new JSONObject(json);
                if (obj.optJSONObject("opds") != null)
                    main.loadCatalog(url, obj, null);
                else
                    app.engines.addJson(main, url, obj);
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
            return;
        }
    }

    @Override
    public View createView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ProgressBar view = new ProgressBar(inflater.getContext());
        view.setIndeterminate(true);

        // wait until torrent loaded
        setCancelable(false);

        //getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return view;
    }
}
